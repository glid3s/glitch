import uuid
from glitch.core.teamserver.crypto import gen_stager_psk
from glitch.core.teamserver.stager import Stager
from glitch.core.utils import get_path_in_package


class glitchStager(Stager):
    def __init__(self):
        self.name = 'dll'
        self.description = 'Generates a windows dll stager'
        self.suggestions = ''
        self.extension = 'dll'
        self.author = '@byt3bl33d3r'
        self.options = {}

    def generate(self, listener):
        with open(get_path_in_package('core/teamserver/data/glider.dll'), 'rb') as dll:
            guid = uuid.uuid4()
            psk = gen_stager_psk()

            return guid, psk, dll.read().decode('latin-1')
