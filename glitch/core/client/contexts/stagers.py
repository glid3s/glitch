import asyncio
import logging
from terminaltables import SingleTable
from termcolor import colored
from glitch.core.utils import print_good
from glitch.core.client.utils import command, register_cli_commands

@register_cli_commands
class Stagers:
    name = 'stagers'
    description = 'Stagers menu'

    _remote = True

    def __init__(self):
        self.prompt = None
        self.available = []
        self._selected = None

    @property
    def selected(self):
        return self._selected

    @selected.setter
    def selected(self, data):
        self.prompt = f"(<ansired>{data['name']}</ansired>)"
        self._selected = data

    @command
    def use(self, name: str, response):
        """
        Select the specified stager

        Usage: use <name> [-h]

        Arguments:
            name  filter by stager name
        """

        self.selected = response.result

    @command
    def list(self, response):
        """
        List available stagers

        Usage: list [-h]
        """
        table_data = [
            [colored("Name", "white", attrs=['bold']), colored("Description", "white", attrs=['bold'])]
        ]
        for name,fields in response.result.items():
            table_data.append([colored(name, "blue"), colored(fields["description"], "green")])

        table = SingleTable(table_data)
        #table.inner_row_border = False
        table.inner_column_border = False
        #table.inner_footing_row_border = False
        table.inner_heading_row_border = False
        table.outer_border = False
        print("\n" + table.table + "\n")

    @command
    def options(self, response):
        """
        Show selected stager options

        Usage: options [-h]
        """

        table_data = [[colored("Option Name", "white", attrs=['bold']), colored("Required", "white", attrs=['bold']), colored("Value", "white", attrs=['bold']), colored("Description", "white", attrs=['bold'])]]
        for k, v in response.result.items():
            table_data.append([colored(k,"white"), colored(v["Required"],"white"), colored(v["Value"], "blue"), colored(v["Description"], "green")])

        table = SingleTable(table_data)
        #table.inner_row_border = False
        table.inner_column_border = False
        #table.inner_footing_row_border = False
        table.inner_heading_row_border = False
        table.outer_border = False
        print("\n" + table.table + "\n")

    @command
    def generate(self, listener_name: str, response):
        """
        Generate the selected stager

        Usage: generate [-h] <listener_name>

        Arguments:
            listener_name   listener name
        """

        generated_stager = response.result

        stager_filename = f"./stager.{generated_stager['extension']}"
        with open(stager_filename, 'wb') as stager:
            stager.write(generated_stager['output'].encode('latin-1'))

        print_good(f"Generated stager to {stager_filename}")

    @command
    def set(self, name: str, value: str, response):
        """
        Set options on the selected listener

        Usage: set <name> <value> [-h]

        Arguments:
            name   option name
            value  option value
        """

    @command
    def reload(self, response):
        """
        Reload all modules

        Usage: reload [-h]
        """
