import asyncio
import logging
from glitch.core.utils import print_good, print_info, print_bad
from glitch.core.client.utils import command, register_cli_commands
from terminaltables import SingleTable
from termcolor import colored
from time import gmtime, strftime

@register_cli_commands
class Sessions:
    name = 'sessions'
    description = 'Sessions menu'

    _remote = True

    def __init__(self):
        self._selected = None
        self.prompt = None

    @property
    def selected(self):
        return self._selected

    @selected.setter
    def selected(self, data):
        #self.prompt = f"(<ansired>{data['name']}</ansired>)"
        self._selected = data

    @command
    def purge(self, response):
        """
        Purge old sessions

        Usage: purge [-h]
        """

        print_good(f"Purged {response.result} sessions.")

    @command
    def list(self, response):
        """
        Get available sessions

        Usage: list [-h]
        """

        table_data = [
            [
                colored("Name", "white", attrs=['bold']),
                colored("PID", "white", attrs=['bold']),
                colored("User", "white", attrs=['bold']),
                colored("Address", "white", attrs=['bold']),
                colored("Last Checkin", "white", attrs=['bold'])
            ]
        ]

        for guid, session in response.result.items():
            if session['info']:
                try:
                    username = f"{session['info']['Domain']}\\{session['info']['Username']}@{session['info']['Hostname']}"
                    if session['info']['HighIntegrity']:
                        username = f"*{username}"
                        username = colored(username, "red")
                except KeyError:
                    username = 'N/A'

                if (gmtime(session['lastcheckin'])[5] > int(session['info']['Sleep']/1000)):
                    timestamp = colored(strftime("%Hh %Mm %Ss", gmtime(session['lastcheckin'])), "red", attrs=['bold'])
                else:
                    timestamp = colored(strftime("%Hh %Mm %Ss", gmtime(session['lastcheckin'])), "green", attrs=['bold'])
                table_data.append([
                    colored(guid, "white"),
                    colored(session['info']['ProcessId'], "yellow"),
                    username,
                    colored(session['address'], "white"),
                    timestamp
                ])

        table = SingleTable(table_data)
        #table.inner_row_border = True
        table.inner_column_border = False
        #table.inner_footing_row_border = False
        table.inner_heading_row_border = False
        table.outer_border = False
        print("\n" + table.table + "\n")

    @command
    def info(self, guid: str, response):
        """
        Get info of a specified session

        Usage: info [-h] <guid>
        """

        table_data = [[colored("Name", "white", attrs=['bold']), colored("Value", "white", attrs=['bold'])]]
        for k,v in response.result['info'].items():
            table_data.append([colored(k, "blue"), colored(v, "green")])

        table = SingleTable(table_data)
        #table.inner_row_border = True
        table.inner_column_border = False
        #table.inner_footing_row_border = False
        table.inner_heading_row_border = False
        table.outer_border = False
        print("\n" + table.table + "\n")

    @command
    def kill(self, guid: str, response):
        """
        Kill a session

        Usage: kill [-h] <guid>
        """

        print_info(f"Tasked session {guid} to exit")

    @command
    def sleep(self, guid: str, interval: int, response):
        """
        Modify a sessions check-in interval in ms

        Usage: sleep [-h] <guid> <interval>
        """

        #print_good(f"Session {guid} will now check in every {interval}ms")

    @command
    def jitter(self, guid: str, max: int, min: int, response):
        """
        Modify a sessions jitter value in ms

        Usage: jitter [-h] <guid> <max> [<min>]
        """

        #print_good(f"Session {guid} will now have a max jitter of {max}ms and a min jitter of {min}ms")

    @command
    def register(self, guid: str, psk: str, response):
        """
        Register a session with the server

        Usage: register [-h] [<guid>] [<psk>]
        """

        print_good(f"Registered new session (guid: {response.result['guid']} psk: {response.result['psk']})")

    @command
    def unregister(self, guid: str, response):
        """
        Unregister a session with the server

        Usage: unregister [-h] [<guid>]
        """
        try:
            if (response.result['guid']):
                print_good(f"Unregistered session (guid: {response.result['guid']})")
        except KeyError:
            print_bad(f"{response.result['message']}")

    @command
    def getpsk(self, guid: str, response):
        """
        Get the psk of a session

        Usage: getpsk [-h] [<guid>]
        """

        print_good(f"psk: {response.result['psk']}")

    @command
    def checkin(self, guid: str, response):
        """
        Force a session to checkin

        Usage: checkin [-h] <guid>
        """
        pass

    @command
    def rename(self, guid: str, name: str, response):
        """
        Give a session a friendly name

        Usage: rename [-h] <guid> <name>
        """
        pass
