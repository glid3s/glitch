import asyncio
import logging
from typing import List
from glitch.core.client.utils import command, register_cli_commands
from terminaltables import SingleTable
from termcolor import colored
from time import gmtime, strftime

@register_cli_commands
class Modules:
    name = 'modules'
    description = 'Modules menu'

    _remote = True

    def __init__(self):
        self.prompt = None
        self.available = []
        self._selected = None

    @property
    def selected(self):
        return self._selected

    @selected.setter
    def selected(self, data):
        self.prompt = f"(<ansired>{data['name']}</ansired>)"
        self._selected = data

    @command
    def use(self, name: str, response):
        """
        Select the specified module

        Usage: use <name> [-h]

        Arguments:
            name  module to select
        """

        self.selected = response.result

    @command
    def list(self, name: str, response):
        """
        Get available modules

        Usage: list [-h] [<name>]

        Arguments:
            name   filter by module name
        """

        table_data = [[colored('Name', "white", attrs=['bold']), colored('Description', "white", attrs=['bold'])]]
        for m_name, m_description in response.result.items():
            table_data.append([colored(m_name, "blue"), colored(m_description, "green")])

        table = SingleTable(table_data)
        #table.inner_row_border = False
        table.inner_column_border = False
        #table.inner_footing_row_border = False
        table.inner_heading_row_border = False
        table.outer_border = False
        print("\n" + table.table + "\n")

    @command
    def options(self, response):
        """
        Show selected module options

        Usage: options [-h]
        """

        table_data = [[colored("Option Name", "white", attrs=['bold']), colored("Required", "white", attrs=['bold']), colored("Value", "white", attrs=['bold']), colored("Description", "white", attrs=['bold'])]]
        for k, v in response.result.items():
            table_data.append([colored(k,"white"), colored(v["Required"],"white"), colored(v["Value"], "blue"), colored(v["Description"], "green")])

        table = SingleTable(table_data)
        #table.inner_row_border = False
        table.inner_column_border = False
        #table.inner_footing_row_border = False
        table.inner_heading_row_border = False
        table.outer_border = False
        print("\n" + table.table + "\n")

    @command
    def info(self, response):
        """
        Show detailed information of the selected module

        Usage: options [-h]
        """
        print(f"Author(s): {response.result['author']}")
        print(f"Description: {response.result['description']}")
        print(f"Language: {response.result['language']}\n")

        table_data = [[colored("Option Name", "white", attrs=['bold']), colored("Required", "white", attrs=['bold']), colored("Value", "white", attrs=['bold']), colored("Description", "white", attrs=['bold'])]]
        for k, v in response.result['options'].items():
            table_data.append([colored(k,"white"), colored(v["Required"],"white"), colored(v["Value"], "blue"), colored(v["Description"], "green")])

        table = SingleTable(table_data)
        #table.inner_row_border = False
        table.inner_column_border = False
        #table.inner_footing_row_border = False
        table.inner_heading_row_border = False
        table.outer_border = False
        print("\n" + table.table + "\n")

    @command
    def run(self, guids: List[str], response):
        """
        Run a module

        Usage:
            run <guids>...
            run -h | --help

        Arguments:
            guids    session guids to run modules on
                     (specifying 'all' will run module on all sessions)

        Options:
            -h, --help   Show dis
        """
        pass

    @command
    def reload(self, response):
        """
        Reload all modules

        Usage: reload [-h]
        """
        pass

    @command
    def set(self, name: str, value: str, response):
        """
        Set options on the selected module

        Usage: set <name> <value> [-h]

        Arguments:
            name   option name
            value  option value
        """
