#! /usr/bin/env python3

from glitch.__main__ import run

if __name__ == '__main__':
    run()
